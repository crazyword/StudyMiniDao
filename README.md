#StudyMiniDao
0、FwDao v0.1 20141025<br>
1、此项目是学习并模仿MiniDao编写的持久层框架演示版<br>
2、目前支持Hibernate和自行编写sql模板两种方式或者两种方式并用<br>
3、sql模板方式使用Ognl表达式和freemarker语法<br>
4、sql模板方式暂不支持返回List和Map<br>
5、暂时未在容错性上投入精力，仅保证测试过的方法能够完整演示<br>
6、精力所限，直接使用了MiniDao的部分工具类和部分表达式解析代码<br>