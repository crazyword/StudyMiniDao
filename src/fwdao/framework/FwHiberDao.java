package fwdao.framework;

import java.io.Serializable;
import java.util.List;

public interface FwHiberDao<T> {
	
	public static final String METHOD_SAVE_BY_HIBER = "saveByHiber";
	public static final String METHOD_GET_BY_ID_HIBER = "getByIdHiber";
	public static final String METHOD_GET_BY_ENTITY_HIBER = "getByEntityHiber";
	public static final String METHOD_UPDATE_BY_HIBER = "updateByHiber";
	public static final String METHOD_DELETE_BY_HIBER = "deleteByHiber";
	public static final String METHOD_LIST_BY_HIBER = "listByHiber";
	public static final String METHOD_DELETE_BY_ID_HIBER = "deleteByIdHiber";
	
	public void saveByHiber(T entity);

	public <T> T getByIdHiber(Class<T> entityClass, final Serializable id);

	public <T> T getByEntityHiber(T entity);

	public void updateByHiber(T entity);

	public void deleteByHiber(T entity);

	public List<T> listByHiber(T entity);

	/**
	 * 根据主键删除指定的实体
	 * 
	 * @param <T>
	 * @param pojo
	 */
	public <T> void deleteByIdHiber(Class entityName, Serializable id);
}
