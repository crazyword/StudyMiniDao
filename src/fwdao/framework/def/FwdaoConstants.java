package fwdao.framework.def;

public class FwdaoConstants {

	/**
	 * 接口方法定義規則
	 * 添加：insert,add,create
	 * 添加：update,modify,store
	 * 刪除：delete,remove
	 * 檢索：以上各单词之外
	 */
	
	public static final String INF_METHOD_ACTIVE = "insert,add,create,update,modify,store,delete,remove";
	public static final String INF_METHOD_BATCH = "batch";
}