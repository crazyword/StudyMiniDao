package fwdao.framework.aop;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ognl.Ognl;
import ognl.OgnlException;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;

import fwdao.framework.FwHiberDao;
import fwdao.framework.anotation.FwdaoArguments;
import fwdao.framework.anotation.FwdaoResultType;
import fwdao.framework.anotation.FwdaoSql;
import fwdao.framework.def.FwdaoConstants;
import fwdao.framework.hiber.IFwHiberCommonDao;
import fwdao.framework.util.FreemarkerParseFactory;
import fwdao.framework.util.FwdaoUtil;

public class FwdaoHandler implements MethodInterceptor {

	private JdbcTemplate jdbcTemplate;
	private IFwHiberCommonDao fwHiberDao;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	private Logger logger = Logger.getLogger(FwdaoHandler.class);
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Object returnResult = null;

		Method method = invocation.getMethod();
		Object[] args = invocation.getArguments();

		// 判断是否为抽象方法
		if (!isAbstract(method)) {
			return invocation.proceed();
		}

		// 判断是否采用Hibernate实体维护
		Map<String, Object> resultMap = new HashMap<>();
		if (invokeHiber(resultMap, method, args)) {
			return resultMap.get("returnObject");
		}

		// 装载注解的SQL模板和所需参数
		Map<String, Object> sqlParamsMap = new HashMap<>();
		String templateSql = prepareParameters(method, args, sqlParamsMap);

		// 解析SQL模板，返回可执行SQL
		String executeSql = parseSqlTemplate(method, templateSql, sqlParamsMap);

		// 组装SQL占位符参数
		Map<String, Object> sqlMap = installPlaceholderSqlParam(executeSql,
				sqlParamsMap);

		// 获取SQL执行返回值
		returnResult = getReturnFwdaoResult(jdbcTemplate, method, executeSql, sqlMap);

		return returnResult;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public IFwHiberCommonDao getFwHiberDao() {
		return fwHiberDao;
	}

	public void setFwHiberDao(IFwHiberCommonDao fwHiberDao) {
		this.fwHiberDao = fwHiberDao;
	}

	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	/**
	 * 判断方法是否是抽象方法
	 * 
	 * @param method
	 * @return
	 */
	public boolean isAbstract(Method method) {
		int mod = method.getModifiers();
		return Modifier.isAbstract(mod);
	}

	public boolean invokeHiber(Map resultMap, Method method, Object[] args) {

		Class<?> clz = null;

		switch (method.getName()) {
		case FwHiberDao.METHOD_DELETE_BY_HIBER:
			fwHiberDao.delete(args[0]);
			return true;

		case FwHiberDao.METHOD_DELETE_BY_ID_HIBER:
			clz = (Class<?>) args[0];
			fwHiberDao.deleteEntityById(clz, args[1].toString());
			return true;

		case FwHiberDao.METHOD_GET_BY_ENTITY_HIBER:
			resultMap.put("returnObject", fwHiberDao.get(args[0]));
			return true;

		case FwHiberDao.METHOD_GET_BY_ID_HIBER:
			clz = (Class<?>) args[0];
			resultMap.put("returnObject",
					fwHiberDao.get(clz, args[1].toString()));
			return true;

		case FwHiberDao.METHOD_LIST_BY_HIBER:
			resultMap.put("returnObject", fwHiberDao.loadAll(args[0]));
			return true;

		case FwHiberDao.METHOD_SAVE_BY_HIBER:
			fwHiberDao.save(args[0]);
			return true;

		case FwHiberDao.METHOD_UPDATE_BY_HIBER:
			fwHiberDao.saveOrUpdate(args[0]);
			return true;
		}

		return false;
	}

	/**
	 * 为组装sql准备参数，暂不支持分页，请待后续升级。
	 * 
	 * @param method
	 * @param args
	 * @param sqlParamsMap
	 *            传出参数
	 * @return 取得的sql模板
	 */
	private String prepareParameters(Method method, Object[] args,
			Map<String, Object> sqlParamsMap) {
		if(method.isAnnotationPresent(FwdaoArguments.class)){
			String[] annoArgs = method.getAnnotation(FwdaoArguments.class).value();
			
			//TODO: 检查注解使用
			
			//暂时假定所有参数都要求注解，并且假定注解正确使用
			for(int i=0; i<annoArgs.length; i++){
				sqlParamsMap.put(annoArgs[i], args[i]);
			}
			
		}
		
		String annoSqlTemplate = null;
		if(method.isAnnotationPresent(FwdaoSql.class)){
			FwdaoSql sql = method.getAnnotation(FwdaoSql.class);
			if(StringUtils.isNotEmpty(sql.value())){
				annoSqlTemplate = sql.value();
			}
		}
		return annoSqlTemplate;
	}

	/**
	 * 解析SQL模板
	 * 
	 * @param method
	 * @param templateSql
	 * @param sqlParamsMap
	 * @return  可执行SQL
	 */
	private String parseSqlTemplate(Method method,String templateSql,Map<String,Object> sqlParamsMap){
		// step.1.根据命名规范[接口名_方法名.sql]，获取SQL模板文件的路径
		String executeSql = null;
		
		// step.2.获取SQL模板内容
		// step.3.通过模板引擎给SQL模板装载参数,解析生成可执行SQL
		if(StringUtils.isNotEmpty(templateSql)){
			executeSql = new FreemarkerParseFactory().parseTemplateContent(templateSql, sqlParamsMap);
		}else{
			// update-begin--Author:fancq  Date:20131225 for：sql放到dao层同样目录
			// update-begin--Author:zhaojunfu  Date:20140418 for：扫描规则-首先扫描同位置sql目录,如果没找到文件再搜索dao目录
			String sqlTempletPath = "/"+method.getDeclaringClass().getName().replace(".", "/").replace("/dao/", "/sql/")+"_"+method.getName()+".sql";
			URL sqlFileUrl = this.getClass().getClassLoader().getResource(sqlTempletPath);
			if(sqlFileUrl==null){
				sqlTempletPath = "/"+method.getDeclaringClass().getName().replace(".", "/")+"_"+method.getName()+".sql";
			}
			// update-end--Author:fancq  Date:20131225 for：sql放到dao层同样目录
			// update-end--Author:zhaojunfu  Date:20140418 for：扫描规则：首先扫描同位置sql目录,如果没找到文件再搜索dao目录
			logger.debug("MiniDao-SQL-Path:"+sqlTempletPath);
			executeSql = new FreemarkerParseFactory().parseTemplate(sqlTempletPath, sqlParamsMap);
		}
		return getSqlText(executeSql);
	}
	
	/**
	 * 除去无效字段，不然批量处理可能报错
	 */
	private String getSqlText(String sql) {
		return sql.replaceAll("\\n", " ").replaceAll("\\t", " ")
				.replaceAll("\\s{1,}", " ").trim();
	}
	
	/**
	 * 组装占位符参数 -> Map
	 * @param executeSql
	 * @return
	 * @throws OgnlException 
	 */
	private Map<String,Object> installPlaceholderSqlParam(String executeSql,Map sqlParamsMap) throws OgnlException{
		Map<String,Object> map = new HashMap<String,Object>();
		String regEx = ":[ tnx0Bfr]*[0-9a-z.A-Z]+"; // 表示以：开头，[0-9或者.或者A-Z大小都写]的任意字符，超过一个
		Pattern pat = Pattern.compile(regEx);
		Matcher m = pat.matcher(executeSql);
		while (m.find()) {
			logger.debug(" Match [" + m.group() +"] at positions " + m.start() + "-" + (m.end() - 1));
			String ognl_key = m.group().replace(":","").trim();
			map.put(ognl_key, Ognl.getValue(ognl_key, sqlParamsMap));
		}
		return map;
	}
	
	/**
	 *  获取Fwdao处理结果集
	 * @param jdbcTemplate
	 * @param method
	 * @param executeSql
	 * @return  结果集
	 */
	private Object getReturnFwdaoResult(JdbcTemplate jdbcTemplate,Method method,String executeSql,Map<String,Object> paramMap){
		// step.4.调用SpringJdbc引擎，执行SQL返回值
		// 5.1获取返回值类型[Map/Object/List<Object>/List<Map>/基本类型]
		String methodName = method.getName();
		// 判斷是否非查詢方法
		if (checkActiveKey(methodName)) {
			if(paramMap!=null){
				return namedParameterJdbcTemplate.update(executeSql, paramMap);
			}else{
				return jdbcTemplate.update(executeSql);
			}
		} else if (checkBatchKey(methodName)) {
			return batchUpdate(jdbcTemplate,executeSql);
		} else {
			// 如果是查詢操作
			Class<?> returnType = method.getReturnType();
			if (returnType.isPrimitive()) {
				Number number = (Number) jdbcTemplate.queryForObject(executeSql,BigDecimal.class);
				if ("int".equals(returnType)) {
					return number.intValue();
				} else if ("long".equals(returnType)) {
					return number.longValue();
				} else if ("double".equals(returnType)) {
					return number.doubleValue();
				}
			} else if (returnType.isAssignableFrom(List.class)) {
				//List类型暂不支持
			} else if (returnType.isAssignableFrom(Map.class)) {
				//Map类型暂不支持
				/*
				if(paramMap!=null){
					return (Map)namedParameterJdbcTemplate.queryForObject(executeSql, paramMap,getColumnMapRowMapper());
				}else{
					return (Map)jdbcTemplate.queryForObject(executeSql,getColumnMapRowMapper());
				}
				*/
			} else if (returnType.isAssignableFrom(String.class)) {
				//String类型
				try{  
					if(paramMap!=null){
						return namedParameterJdbcTemplate.queryForObject(executeSql, paramMap, String.class);
					}else{
						return jdbcTemplate.queryForObject(executeSql,String.class);
					}
		        }catch (EmptyResultDataAccessException e) {  
		            return null;  
		        }
			}else if(FwdaoUtil.isWrapClass(returnType)){
				//基本类型的包装类
				try{  
					if(paramMap!=null){
						return namedParameterJdbcTemplate.queryForObject(executeSql, paramMap, returnType);
					}else{
						return jdbcTemplate.queryForObject(executeSql, returnType);
					}
		        }catch (EmptyResultDataAccessException e) {  
		            return null;  
		        }
			} else {
				// 对象类型
				RowMapper<?> rm = ParameterizedBeanPropertyRowMapper.newInstance(returnType);
				try{  
					if(paramMap!=null){
						return namedParameterJdbcTemplate.queryForObject(executeSql, paramMap, rm);
					}else{
						return jdbcTemplate.queryForObject(executeSql, rm);
					}
		        }catch (EmptyResultDataAccessException e) {  
		            return null;  
		        }
			}
		}
		return null;
	}
	
	/**
	 * 判斷是否是執行的方法（非查詢）
	 * 
	 * @param methodName
	 * @return
	 */
	private static boolean checkActiveKey(String methodName) {
		String keys[] = FwdaoConstants.INF_METHOD_ACTIVE.split(",");
		for (String s : keys) {
			if (methodName.startsWith(s))
				return true;
		}
		return false;
	}
	
	/**
	 * 判斷是否批處理
	 * 
	 * @param methodName
	 * @return
	 */
	private static boolean checkBatchKey(String methodName) {
		String keys[] = FwdaoConstants.INF_METHOD_BATCH.split(",");
		for (String s : keys) {
			if (methodName.startsWith(s))
				return true;
		}
		return false;
	}
	
	/**
	 * 批处理
	 *@Author JueYue
	 *@date   2013-11-17
	 *@return
	 */
	private int[] batchUpdate(JdbcTemplate jdbcTemplate, String executeSql) {
		String[] sqls = executeSql.split(";");
		if(sqls.length<100){
			return jdbcTemplate.batchUpdate(sqls);
		}
		int[] result = new int[sqls.length];
		List<String> sqlList = new ArrayList<String>();
		for(int i = 0;i<sqls.length;i++){
			sqlList.add(sqls[i]);
			if(i%100 == 0){
				addResulArray(result,i+1,jdbcTemplate.batchUpdate(sqlList.toArray(new String[0])));
				sqlList.clear();
			}
		}
		addResulArray(result,sqls.length,jdbcTemplate.batchUpdate(sqlList.toArray(new String[0])));
		return result;
	}


	/**
	 * 把批量处理的结果拼接起来
	 *@Author JueYue
	 *@date   2013-11-17
	 */
	private void addResulArray(int[] result,int index, int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++){
			result[index-length + i] = arr[i];
		}
	}
	
	
}
