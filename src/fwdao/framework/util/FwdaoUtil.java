package fwdao.framework.util;

public class FwdaoUtil {
	public static String getBeanName(String className){
		
		className = className.trim();
		if(className.length()>=2){
			return className.substring(0, 1).toLowerCase()+className.substring(1);
		}else{
			return className.toLowerCase();
		}
		
		
	}
	
	/**
	 * 判断Class是否是基本包装类型
	 * 
	 * @param clz
	 * @return
	 */
	public static boolean isWrapClass(Class clz) {
		try {
			return ((Class) clz.getField("TYPE").get(null)).isPrimitive();
		} catch (Exception e) {
			return false;
		}
	}
}
