package fwdao.framework.factory;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PackageUtil {
	
	public static List<String> getClassNames(String pkgName){
		List<String> result = new ArrayList<>();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();

		String path = pkgName.replace(".", "/");
		
		URL url = loader.getResource(path);
		String filePath = url.getPath();
		File file = new File(filePath);
		File[] children = file.listFiles();
		for(File child : children){
			if(child.isDirectory()){
				String childPkgName = pkgName + "." + child.getName();
				result.addAll(getClassNames(childPkgName));
			}else{
				result.add(pkgName + "." + child.getName().replace(".class", ""));
			}
		}
		
		return result;
	}
}
