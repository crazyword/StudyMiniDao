package fwdao.framework.factory;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import fwdao.framework.anotation.Fwdao;
import fwdao.framework.util.FwdaoUtil;

public class FwdaoBeanFactory implements BeanFactoryPostProcessor {

	private List<String> packagesToScan;

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory clBeanFactory)
			throws BeansException {
		Logger logger = Logger.getLogger(FwdaoBeanFactory.class);
		//对多个配置的包名循环
		for(String pkgName: packagesToScan){
			logger.info("Scanning package: "+pkgName + " (by FwdaoBeanFactory)");
			Set<Class<?>> classes = PackagesToScanUtil.getClasses(pkgName);
			//包内对多个类循环
			for(Class<?> clazz : classes){
				logger.info("  Scanning class: "+clazz);
				if(clazz.isAnnotationPresent(Fwdao.class)){
					logger.debug("  Found @Fwdao defination: "+clazz);
					Class fwdaoClass = clazz;
					//创建bean
					ProxyFactoryBean pfb = new ProxyFactoryBean();
					pfb.setBeanFactory(clBeanFactory);
					pfb.setInterfaces(fwdaoClass);
					pfb.setInterceptorNames("fwdaoHandler");
					String beanName = FwdaoUtil.getBeanName(fwdaoClass.getSimpleName());
					//注册bean
					if(!clBeanFactory.containsBean(beanName)){
						clBeanFactory.registerSingleton(beanName, pfb.getObject());
						logger.info("  registered bean: "+beanName);
					}
					
				}
			}
		}

	}

	public List<String> getPackagesToScan() {
		return packagesToScan;
	}

	public void setPackagesToScan(List<String> packagesToScan) {
		this.packagesToScan = packagesToScan;
	}

}
