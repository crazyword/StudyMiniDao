package example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import example.dao.StudentDao;
import example.entity.Student;

@Service
public class StudentService {

	@Autowired
	private StudentDao studentDao;
	
	public void add(String name){
		Student s = new Student();
		s.setName(name);
		studentDao.insert(s);
	}

}
