package example.service;

import org.springframework.beans.factory.annotation.Autowired;

import example.dao.CourseDao;
import example.entity.Course;

import org.springframework.stereotype.Service;

@Service
public class CourseService {
	@Autowired
	private CourseDao courseDao;
	
	public void add(String name){
		Course c = new Course();
		c.setName(name);
		courseDao.saveByHiber(c);
		
	}
}
