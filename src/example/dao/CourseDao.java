package example.dao;

import example.entity.Course;
import fwdao.framework.FwHiberDao;
import fwdao.framework.anotation.Fwdao;

@Fwdao
public interface CourseDao extends FwHiberDao<Course> {

}
