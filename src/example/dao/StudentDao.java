package example.dao;

import example.entity.Student;
import fwdao.framework.anotation.Fwdao;
import fwdao.framework.anotation.FwdaoArguments;

@Fwdao
public interface StudentDao {
	@FwdaoArguments("student")
	public void insert(Student student);
}
